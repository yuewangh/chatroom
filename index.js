

// Using HTTPS
var fs = require('fs');
var https = require('https');

var express = require('express');
var app = express();

var options = {
    key: fs.readFileSync('./certs/file.pem'),
    cert: fs.readFileSync('./certs/file.crt')
};
var serverPort = 443;

var server = https.createServer(options, app);
var io = require('socket.io')(server);

// Using HTTP
//var app = require('express')();
//var server = require('http').Server(app);
//var io = require('socket.io')(server);


var sentiment = require('sentiment');

var rd_cfg = { port: 6379, host: '127.0.0.1' };

var Redis = require('ioredis'),
    redisClient = new Redis(rd_cfg.port, rd_cfg.host);

var negative_sentiment_threshold = -20;

app.get('/', function(req, res){
    res.sendFile(__dirname + '/chat.html');
});

var getRoomSockets = function(room_name) {
    var socketIds = io.sockets.adapter.rooms[room_name]; 
    socketIds = Object.keys(socketIds).map(function(sid) {
        if (socketIds[sid] == true) {
            return sid;
        }
    });
    return socketIds.map(function(id) {
        return io.sockets.connected[id];
    });
};

var getRoomUsers = function(room_name) {
    var sockets = getRoomSockets(room_name);
    var uniq_users = new Map();
    var users = [ ];

    console.log("SOCKETS LENGHT: " + sockets.length);

    for (var i=0; i<sockets.length; i++) {
        if (sockets[i].chat_user.silent) {
            console.log("SKIPPING! 1");
            continue;
        }
        if (uniq_users.has(sockets[i].chat_user.id)) {
            console.log("SKIPPING 2!");
            continue;
        }
        uniq_users.set(sockets[i].chat_user.id, sockets[i].chat_user);
    }

    uniq_users.forEach(function(v,k){
        users.push(v);
    });

    return users;
};

//var getRoomUsers = function(room_name) {
//    var sockets = getRoomSockets(room_name);
//    var users = [ ];
//    for (var i=0; i<sockets.length; i++) {
//        if (sockets[i].chat_user != null) {
//            users.concat(sockets[i].chat_user);
//        }
//    }
//    return users;
//};


var getSocketRoomName = function(socket) {
    console.log("Socket room names: " + JSON.stringify(socket.rooms));
    return socket.rooms[1] || socket.rooms[0];  /* Use room specified on connection, or fallback to default */
}

function redisKeys(room_name) {
    return ['events', 'donations_amount', 'donations_count', 'shares_count', 'sentiment'].reduce(function(map, x) {
        map[x] = x + ':' + room_name;
        return map;
    }, { });
}


//var emitToRoom(room_name, event, msg) {
//    var sockets = getRoomSockets(room_name);
//    for (var i=0; i<sockets.length; i++) {
//        if (!sockets[i].chat_user.silent) {
//            sockets[i].emit(event, msg);
//        }
//    }
//}

// Pass in any number of items:
// {
//     share: 1
//     donation: amount
// }
var emitRoomStats = function(room_name) {
    var rdk = redisKeys(room_name);
    var pipeline = redisClient.pipeline();
    pipeline.get(rdk.donations_amount);
    pipeline.get(rdk.donations_count);
    pipeline.get(rdk.shares_count);
    pipeline.exec(function(err, redis_results) {
        var stats = {
            event:            'stats',
            donations_amount: redis_results[0][1] || 0,
            donations_count:  redis_results[1][1] || 0,
            shares_count:     redis_results[2][1] || 0
        };
        console.log(rdk);
        console.log("STATS!" + JSON.stringify(stats));
        io.in(room_name).emit('stats', stats);
    });
}


if (redisClient.pipeline) {
    console.log("Looks like we have pipeline!");
}
else {
    console.log("BOGUS, no pipeline support!");
}

io.on('connection', function(socket){

    // Clearing all stats for the given room
    socket.on('clear_stats', function(msg){
        console.log("CLEAR STATS: " + JSON.stringify);
        var pipeline = redisClient.pipeline();
        var rdk = redisKeys(msg.room);
        for (var key in rdk) {
            if (rdk.hasOwnProperty(key)) {
                console.log(key + " -> " + rdk[key]);
                pipeline.del(rdk[key]);
            }
        }
        pipeline.exec(function(err, redis_result){});
        io.in(msg.room).emit('clear_stats', { at: (new Date).getTime() });
    });

    // Expect the following donation message:
    //   { event: 'join',
    //     user: { id: 1234, name: 'Bob Bielski'},
    //     events: [
    //       { event: 'chat',
    //           <see chat events>
    //       },
    //        . . .
    //       { event: 'donation',
    //           <see donation events>
    //       }
    //     ]
    //   }
    //
    // Response:
    //   { event: 'donation',
    //     user: { id: 1234, name: 'Bob Bielski'},
    //     text:  'The chat message itself'
    //   }

    socket.on('join', function(msg) {

        socket.join(msg.room);
        socket.chat_user = msg.user;

        var pipeline = redisClient.pipeline();
        var rdk = redisKeys(msg.room);

        console.log("BINGO");

        pipeline.lrange(rdk.events, 0, 20);
        pipeline.ltrim(rdk.events, 0, 20);
        pipeline.exec(function(err, redis_result) {
            console.log("redis keys: " + JSON.stringify(rdk));
            console.log("redis info on connect: " + JSON.stringify(redis_result));
            console.log(redis_result[0]);
            var to_send = {
                at: (new Date).getTime(),
                user: msg.user,
                users: getRoomUsers(msg.room) || [ ],
                events: (redis_result[0][1] || [ ]).map(function(x) { try { return JSON.parse(x); } catch(err) { } })
            }
            console.log(to_send);
            if (msg.user.silent) {
                console.log("SILENT");
                socket.emit('join', to_send);
            }
            else {
                console.log("LOUD");
                io.in(msg.room).emit('join', to_send);
            }
            console.log(msg);
            emitRoomStats(msg.room);
        });

    });

    // Expect the following donation message:
    //   { event: 'chat',
    //     text: "The chat message itself"
    //   }
    //
    // Response:
    //   { event: 'donation',
    //     user: { id: 1234, name: 'Bob Bielski'},
    //     text:  'The chat message itself'
    //   }

    socket.on('chat', function(msg){
        var room_name = getSocketRoomName(socket);
        var rdk = redisKeys(room_name);

        console.log("RDK: " + JSON.stringify(rdk));

        console.log("writing to redis: " + rdk.events + ', ' + JSON.stringify(msg));
        msg.event = "chat";
        msg.user  = socket.chat_user;
        msg.sentiment = sentiment(msg.text, { bogus: -5, lying: -5, bastard: -10, liar: -10, faking: -10, fake: -10, untrustworthy: -10, bullshit: -10, cancer: 1, disease: 1, emergency: 5, deserving: 10, asshole: -10, hate: -5, hateful: -5 });

        redisClient.lpush(rdk.events, JSON.stringify(msg));

        msg.at = (new Date).getTime();

        console.log("Just got this message: " + JSON.stringify(msg));

        io.in(room_name).emit('chat', msg);

        var pipeline = redisClient.pipeline();
        pipeline.get(rdk.sentiment);
        pipeline.incrby(rdk.sentiment, msg.sentiment.score);
        pipeline.exec(function(err, redis_results){
            var prev_sentiment = redis_results[0][1] || 0;
            var cur_sentiment  = redis_results[1][1] || 0;
            console.log("SENTIMENT: " + prev_sentiment+ ' --> ' + cur_sentiment);

        });
    });

    // Expect the following donation message:
    //   { event: 'donation',
    //     user: { id: 1234, name: 'Bob Bielski'},
    //     donation: { amount: 123 }
    //   }
    //
    // Response:
    //   { event: 'donation',
    //     user: { id: 1234, name: 'Bob Bielski'},
    //     donation: { amount: 123 },
    //     donations_amount: <total among friends>,
    //     donations_count:  <total count among friends>
    //   }

    socket.on('donation', function(msg){
        var room_name = getSocketRoomName(socket);
        var rdk = redisKeys(room_name);

        msg.event = 'donation';
        var pipeline = redisClient.pipeline();
        pipeline.incrby(rdk.donations_amount, msg.donation.amount);
        pipeline.incr(rdk.donations_count);
        pipeline.exec(function(err, redis_results){
            console.log(err);
            console.log("REDIS RESULT: " + JSON.stringify(redis_results));
            msg.donations_amount = redis_results[0][1];
            msg.donations_count  = redis_results[1][1];
            msg.user             = socket.chat_user;
            msg.at = (new Date).getTime();
            io.in(room_name).emit('donation', msg);
            console.log("Donation: " + JSON.stringify(msg));
            redisClient.lpush(rdk.events, JSON.stringify(msg));
            emitRoomStats(room_name);
        });
    });

    // Expect the following share message:
    //   { event: 'share',
    //     user: { id: 1234, name: 'Bob Bielski'},
    //   }

    socket.on('share', function(msg){
        var room_name = getSocketRoomName(socket);
        var rdk = redisKeys(room_name);

        msg.user = socket.chat_user;
        msg.event = 'share';  /* being safe */
        msg.at = (new Date).getTime();
        redisClient.incr(rdk.shares_count, function(a){
            io.in(room_name).emit('share', msg);
            emitRoomStats(room_name);
        });
    });

});



var port = process.env.PORT || 3000;

server.listen(port, function(){
    console.log('listening on *:' + port);
});
